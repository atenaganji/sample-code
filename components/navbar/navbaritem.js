import React from 'react';
import { Component } from 'react';
//import './navbar.css'
import { NavItem, } from 'reactstrap';
import  Link from "next/link";

class NavBarItem extends Component {
  constructor(props) {
    super(props);
    state: {
    }
  }
  render() {
    return (
      <NavItem className={`px-0 px-lg-2`} onClick={this.props.click}>
        <Link href={this.props.href}>
        <a className={`nav-link ${this.props.class}`} >{this.props.content}
        </a>
        </Link>
      </NavItem>
    )
  }
}
export default NavBarItem;
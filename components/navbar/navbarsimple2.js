import React, { Component, useState } from 'react';
import Navitem from './navbaritem'
//import './navbar.css'
import Brand from "./../brand/brand"
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
} from 'reactstrap';
import { BrowserRouter as Router, Link, NavLink, } from "react-router-dom";

class Navbarsimple2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      sideLeft: [
        {
          name: 'How it works',
          path: ''
        },
        {
          name: 'Advantages',
          path: ''
        },
        {
          name: 'Tracking',
          path: '/tracking'
        },
        {
          name: 'Contact Us',
          path: '/contact-us'
        },
        {
          name: 'About Us',
          path: '/about-us'
        }
      ],

    }
  }


  toggle = () => {


    this.setState({ isOpen: !this.state.isOpen });

  }

  componentDidMount(){
    document.querySelector('body').classList.remove('offcanvas-active')
  }


  render() {
    return (

      <Navbar className={`navBar-simple navBar-simple2 w-100 ${this.props.color}`} expand='md'>
       <NavbarBrand href='/' className="brand "><Brand /></NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse className="" isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto " navbar>

            {
              this.state.sideLeft.map((element, index) => {
                return (<Navitem key={index} href={element.path} class="navLink" content={element.name} />)

              })
            }


          </Nav>
        </Collapse>
      </Navbar>
    )
  }

}
export default Navbarsimple2;
import React, {Component} from 'react';
import Navitem from './navbaritem'
//import './navbar.css'
import Brand from "./../brand/brand"
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,

} from 'reactstrap';
import * as actionTypes from './../../store/actions/auth'
import {connect} from 'react-redux'
import axios from './../../axios/axios'
import * as Urls from './../../reqURL/url'

class Navbaritems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            sideLeft: [
                {
                    name: 'How it works',
                    path: '',

                },
                {
                    name: 'Advantages',
                    path: ''
                },
                {
                    dropdown: true,
                    name: 'Services',
                    sub: [
                        {
                            name: 'Air Frieght',
                            path: '/services/air'
                        },
                        {
                            name: 'Sea Frieght',
                            path: '/services/sea'
                        },
                        {
                            name: 'Road Frieght',
                            path: '/services/road'
                        },
                        {
                            name: 'Custom',
                            path: '/services/custom'
                        },
                    ]
                },
                {
                    name: 'Tracking',
                    path: '/tracking'
                },
                {
                    name: 'Contact Us',
                    path: '/contact-us'
                },
                {
                    name: 'About Us',
                    path: '/about-us'
                },
                {
                    name: 'News',
                    path: '/news'
                }
            ],

            sideRight: ["Sign in", "Sign up", "Logout"]
        }
    }


    toggle = (event) => {
        var trigger_id = (event.currentTarget).getAttribute('data-trigger');
        document.querySelector(trigger_id).classList.toggle("show");
        document.querySelector('body').classList.add("offcanvas-active");
    }

    btnClose = () => {
        document.querySelector('.navbar-collapse').classList.remove('show')
        document.querySelector('body').classList.remove('offcanvas-active')
    }

    // close button

    componentDidMount() {
        document.querySelector('body').classList.remove('offcanvas-active')
        window.addEventListener('resize', this.reSize);
        window.addEventListener('scroll', this.handleScroll);
        let dropnav = document.querySelector('.dropnav')
        let navbarlight = document.querySelector('.light-nav .navBar')
        if (window.innerWidth <= 992) {
            if (navbarlight) {
                navbarlight.classList.add('navbar-dark')
                navbarlight.classList.remove('navbar-light')
            }

        } else {
            if (navbarlight) {
                navbarlight.classList.remove('navbar-dark')
                navbarlight.classList.add('navbar-light')
            }
        }

        axios.get(Urls.institute.services).then(response => {
            if (response.data.length != 0 || response.data != null) {
                let fieldIndex = this.state.sideLeft.findIndex(field => {
                    return field.name === 'Services'
                })
                const items = [...this.state.sideLeft]
                const services = []
                response.data.map((element, index) => {
                    services.push({
                        name: element.title,
                        path: `/services/post/?id=${element.id}`,
                    })
                    items[fieldIndex].sub = services
                })
                this.setState({items: items})
            }
        }).catch(error => {

        })

    }

    reSize = () => {
        let navbar = document.querySelector('.navBar')
        let navbarlight = document.querySelector('.light-nav .navBar')
        if (window.innerWidth <= 992) {
            if (navbarlight) {
                navbarlight.classList.add('navbar-dark')
                navbarlight.classList.remove('navbar-light')
            }

        } else {
            if (navbarlight) {
                navbarlight.classList.remove('navbar-dark')
                navbarlight.classList.add('navbar-light')
            }
        }
    }

    componentDidUpdate() {
        let navbar = document.querySelector('.navBar')
        let dropnav = document.querySelector('.dropnav')
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = (event) => {
        let navbar = document.querySelector('.navBar')
        let nav = document.querySelector('.navBar .navbar-nav')
        let dropnav = document.querySelector('.dropnav')
        let navbarlight = document.querySelector('.light-nav .navBar')
        if (window.scrollY > 100) {
            navbar.classList.add('scrollnav')
            nav.classList.add('scrollpadding')
            if (navbarlight) {
                navbarlight.classList.add('navbar-dark')
                navbarlight.classList.remove('navbar-light')
            }
            dropnav.classList.add('scrollnav')
        } else {
            navbar.classList.remove('scrollnav')
            nav.classList.remove('scrollpadding')
            if (navbarlight) {
                navbarlight.classList.remove('navbar-dark')
                navbarlight.classList.add('navbar-light')
                {
                    this.reSize()
                }
            }
        }
    }

    checkLogin = () => {
        if (this.props.isAuthenticated) {
            return (<Navitem class='login ' content={this.state.sideRight[2]} click={this.props.authRemove}/>)
        } else {
            return (<Navitem class='login ' content={this.state.sideRight[0]} href='/accounts/sign-in'/>)
        }
    }


    render() {
        return (
            <header>
                <Navbar className={`navBar w-100  ${this.props.color}`} expand="lg">
                    <NavbarBrand className="brand"><Brand/></NavbarBrand>
                    <NavbarToggler onClick={(event) => this.toggle(event)} data-trigger="#main_nav"/>
                    <Collapse id="main_nav" navbar>
                        <div className="offcanvas-header mt-3">
                            <button className="btn btn-danger btn-close float-right"
                                    onClick={this.btnClose}>&times; Close
                            </button>
                            <NavbarBrand href='/home' className="brand"><Brand/></NavbarBrand>
                        </div>
                        <Nav className="w-100" navbar>
                            {
                                this.state.sideLeft.map((element, index) => {
                                    if (element.dropdown) {
                                        return (
                                            <UncontrolledDropdown key={index} className='  px-0 px-lg-2' nav inNavbar>
                                                <DropdownToggle nav caret>
                                                    {element.name}
                                                </DropdownToggle>
                                                <DropdownMenu className={`dropnav scrollnav`}>
                                                    {
                                                        element.sub.map((element, index) => {
                                                            return (<DropdownItem key={index}>
                                                                <a type='button' className="nav-link navLink"
                                                                   href={element.path}
                                                                > {element.name}</a>
                                                            </DropdownItem>)
                                                        })
                                                    }
                                                </DropdownMenu>
                                            </UncontrolledDropdown>)
                                    } else {
                                        return (<Navitem key={index} href={element.path} class="navLink"
                                                         content={element.name}/>)
                                    }
                                })
                            }
                            <div className='ml-lg-auto d-flex justify-content-center'>
                                <Navitem class="signup justify-content-center mr-2" href='/accounts/sign-up'
                                         content={this.state.sideRight[1]}/>
                                {
                                    this.checkLogin()
                                }
                            </div>
                        </Nav>
                    </Collapse>
                </Navbar>
            </header>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        authen: state.auth.authen,
        isAuthenticated: state.auth.token !== null
    }
}
const mapDispatchToProps = dispatch => {
    return {
        authRemove: () => dispatch({type: actionTypes.AUTH_REMOVE})
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Navbaritems);
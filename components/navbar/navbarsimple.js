import React, { Component, useState } from 'react';
import Navitem from './navbaritem'
//import './navbar.css'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
} from 'reactstrap';


class Navbarsimple extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      sideLeft: [
        {
          name: 'How it works',
          path: ''
        },
        {
          name: 'Advantages',
          path: ''
        },
        {
          name: 'Tracking',
          path: '/tracking'
        },
        {
          name: 'Contact Us',
          path: '/contact-us'
        },
        {
          name: 'About Us',
          path: '/about-us'
        }
      ],

    }
  }
  componentDidMount(){
    document.querySelector('body').classList.remove('offcanvas-active')
  }


  toggle = () => {


    this.setState({ isOpen: !this.state.isOpen });

  }



  render() {
    return (

      <Navbar className={`navBar-simple w-100 ${this.props.color}`} expand='lg'>

        <NavbarToggler onClick={this.toggle} />
        <Collapse className="w-100" isOpen={this.state.isOpen} navbar>
          <Nav className="custom-navbar " navbar>

            {
              this.state.sideLeft.map((element, index) => {
                return (<Navitem key={index} href={element.path} class="navLink" content={element.name} />)

              })
            }


          </Nav>
        </Collapse>
      </Navbar>
    )
  }

}
export default Navbarsimple;
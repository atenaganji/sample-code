import React, { useState, Component } from 'react';
import images from './../../public/assets/images'
import style from './scroll.module.scss'

const scroll = () => {
    return (
        <div className={style.scroll} ><p className={style.scroll}>Scroll </p><img src={images.defaultIcons.arrowDown}></img></div>
    )
}
export default scroll
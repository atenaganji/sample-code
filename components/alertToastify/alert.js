import React, { useState, Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const alert = (type,content,position,delay) => {
  
    switch (type) {
        case 'success': 
         toast.configure();
            return (
          
                toast.success(content, {
                    position: position,
                    autoClose: delay,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            );
        case 'error':
            alert('2')
            toast.configure();
           return (
                toast.error(content, {
                    position: position,
                    autoClose: delay,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            );
        case 'warn':
            toast.configure();
            return (
                toast.warn(content, {
                    position:position,
                    autoClose: delay,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                })
            );
            case 'info':
                toast.configure();
                return (
                    toast.info(content, {
                        position:position,
                        autoClose: delay,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    })
                );
    }
}
export default alert

import React, {Component} from 'react';
import Navbar from "../navbar/navbarsimple"
import Link from 'next/link'

class Welcome extends Component {
    constructor(props) {
        super(props)
    }

    renderBtn = () => {
        if (this.props.btn == true) {
            return (<Link href={this.props.href}>
                <a>{this.props.btnText}</a>
            </Link>)
        }
    }

    render() {
        return (

            <div className="welcome">
                <Navbar color={this.props.color_nav} expand='md'/>

                <div className="text-div">
                    <img src={this.props.src} alt=""/>
                    <h1 className={this.props.colorh}>{this.props.heading}</h1>
                    <p className={this.props.colort}>{this.props.text}</p>
                    <p className={this.props.colort}>{this.props.text2}</p>
                    {
                        this.renderBtn()
                    }

                </div>
            </div>

        )
    }
}

export default Welcome
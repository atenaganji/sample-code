import React, {Component} from 'react';
import {FormGroup, Label, Input} from 'reactstrap';
import CountrySelector from './../selectCountry/selectCountry'
import PhoneNumber from './../phoneNumberInput/phonenumber'
import style from './inputforms.module.scss'

class InputForms extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (

            <FormGroup className="mb-0 ">
                <Label className='text-nowrap'>{this.props.labelValue}</Label>
                {
                    this.props.name == 'nationality' ?
                        <div><CountrySelector value={this.props.value} Change={this.props.change}/><small
                            id={`${this.props.name}-check-alert`}
                            className="form-text-error text-nowrap text-left text-danger"
                            style={{opacity: "0"}}>
                            <i className="fas fa-exclamation-triangle"></i>
                            {this.props.validation}
                        </small></div>
                        :
                        this.props.name == 'phone' ?
                            <div><PhoneNumber value={this.props.value} Change={this.props.change}/>
                                <small id={`${this.props.name}-check-alert`}
                                       className="form-text-error  text-left text-danger"
                                       style={{opacity: "0"}}>
                                    <i className="fas fa-exclamation-triangle"></i>
                                    {this.props.validation}
                                </small></div>
                            :
                            this.props.type == 'password' ?
                                <div>
                                    <Input type="text" name="fake-user" id="user_fake" autoComplete="new-password"
                                           style={{"display": "none"}}/>
                                    <Input type="password" name="fake-email" id="password_fake"
                                           autoComplete="new-password" style={{"display": "none"}}/>
                                    <Input id={this.props.name} autoComplete="new-password"
                                           className="empty-input input" type={this.props.type} name={this.props.name}
                                           placeholder={this.props.placeholder} value={this.props.value}
                                           onChange={this.props.change} required={this.props.required}/>
                                    <small id={`${this.props.name}-check-alert`}
                                           className="form-text-error  text-left text-danger"
                                           style={{opacity: "0"}}>
                                        <i className="fas fa-exclamation-triangle"></i>
                                        {this.props.validation}
                                    </small>
                                </div> :
                                <div>
                                    {this.props.maxlength != null ?
                                        <Input id={this.props.name} maxLength={this.props.maxlength}
                                               className="empty-input input" type={this.props.type}
                                               name={this.props.name} placeholder={this.props.placeholder}
                                               value={this.props.value} onChange={this.props.change}
                                               required={this.props.required}/>
                                        :
                                        <Input id={this.props.name} className="empty-input input" type={this.props.type}
                                               name={this.props.name} placeholder={this.props.placeholder}
                                               value={this.props.value} onChange={this.props.change}
                                               required={this.props.required}/>}

                                    <small id={`${this.props.name}-check-alert`}
                                           className={`${style.formTextError} text-left text-danger`}
                                           style={{opacity: "0"}}>
                                        <i className="fas fa-exclamation-triangle"></i>
                                        {this.props.validation}
                                    </small></div>
                }
            </FormGroup>

        )
    }
}

export default InputForms
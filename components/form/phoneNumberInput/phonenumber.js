import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import React, { Component } from 'react';
class PhoneNumber extends Component {

    constructor(props) {
        super(props)

        this.state = {
            phone: this.props.value,
            defaultValue: 'IR',
            value: '',
        }

    }

    render() {
        return (
            <PhoneInput
                country={'ir'}
                value={this.state.phone}
                onChange={this.props.Change}
            />)
    }

}

export default PhoneNumber
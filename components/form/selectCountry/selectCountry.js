import React, { Component } from 'react';
import countryList from 'react-select-country-list'
import Select from 'react-select/async';

class CountrySelector extends Component {
    constructor(props) {
        super(props)

        this.options = countryList().getData()
        this.findValueIndex = (value) => {
            let label = ''
            this.options.find((element, index) => {
                if (element.value === value) {
                    label = element.label
                }
            })
            return (label)
        }
        this.state = {
            options: this.options,
            defaultValue: 'IR',
            value: this.findValueIndex(this.props.value),
        }

    }

    componentDidMount() {

        let select = document.getElementById('select')


        /*const event = new Event('build');
         // Listen for the event.
         select.addEventListener('build', (e)=> { 
            ()=>{this.changeHandler(this.state.defaultValue)}
            this.props.Change(e)
           alert(this.state.value)
          }, true);
         
         // Dispatch the event.
         select.dispatchEvent(event);*/
    }

    changeHandler = (value) => {

        this.setState({ value: this.findValueIndex(value) })
        //this.props.Change(value,event)

    }
getCountries=()=>{
   return (countryList().getData());
}


    findDefaultIndex = () => {
        let i = 0
        this.state.options.find((element, index) => {
            if (element.value === this.state.defaultValue) {
                i = index
            }
        })
        // this.setState({value:this.state.options[i]})
        return (i)
    }
    setValue = () => {
        this.setState({ value: this.findValueIndex(this.props.value) })
        return this.state.value
    }



    render() {


        return (
            <Select
                className='select'
                id='select'
                inputValue={this.state.value}

                defaultValue={this.state.options[this.findDefaultIndex()]}
                onInputChange={this.changeHandler}
                onChange={this.props.Change}
                defaultOptions={this.state.options}
                loadOptions={this.getCountries}
                cacheOptions

            >
            </Select>


        )
    }
}
export default CountrySelector
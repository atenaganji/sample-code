import React, {Component} from 'react';
import images from './../../public/assets/images'
import style from './brand.module.scss'
import Link from "next/link"

class Brand extends Component {
    constructor(props) {
        super(props)
        this.state = {
            src: images.logo.logo_white
        }
    }

    render() {
        return (
            <Link href="/">
                <span className={style.logo}><img src={this.state.src}></img></span>
            </Link>
        )
    }
}

export default Brand
import React from 'react';
import style from './titleImg.module.scss';


function titleImg(props) {

    return (
        <p className={`${style.titleImg} ${props.class} ${props.colorTitle}`}><span className={props.colorDes}>{props.title}<br />{props.des}</span></p>
    )
}

export default titleImg;
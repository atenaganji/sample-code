import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Column from './column';
import style from  './css/footer.module.scss'
import images from './../../public/assets/images'
import axios from './../../axios/axios'
import * as Urls from './../../reqURL/url'
import { isEmptyObject } from 'jquery';

class footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lists: [
        {
          title: "SERVICES",
          items:
            [
              {
                name: 'Air Frieght',
                path: '/services/air'
              },
              {
                name: 'Sea Frieght',
                path: '/services/sea'
              },
              {
                name: 'Road Frieght',
                path: '/services/road'
              },
              {
                name: 'Custom',
                path: '/services/custom'
              }
            ],
        },
        {
          title: "MENU",
          items:
            [
              {
                name: "How it works",
                path: ''
              },
              {
                name: "Advantages",
                path: ''
              },
              {
                name: "Tecknology",
                path: ''
              },
              {
                name: "About us",
                path: '/about-us'
              }
            ],
        },
      ],
      contactUs:
      {
        title: "CONTACT US",
        items: [[images.footer.mail, "info@skymovex.com"], [images.footer.phone, "+9811111111"], [images.footer.whatsapp, "+9811111111"],[images.footer.map,"No10/4, Adnan Kahveci Mah, , Paşabahçe Cad, Dostlar Beylikdüzü, , Istanbul, Turkey"]],

      },
      followUs: {
        title: "FOLLOW US",
        items: [
          {
            src: images.footer.twitter,
            path: ''
          },
          {
            src: images.footer.facebook,
            path: ''
          },
          {
            src: images.footer.instagram,
            path: ''
          },
          {
            src: images.footer.medium,
            path: ''
          }
        ],
      },
    }
  }
  componentDidMount() {
    axios.get(Urls.home.socialnetworks).then((response) => {
      if (!isEmptyObject(response.data)) {
        const contactus = { ...this.state.contactUs }
        contactus.items[0][1] = response.data.email
        contactus.items[1][1] = response.data.phone_number
        contactus.items[2][1] = response.data.whats_app_phone_number
        contactus.items[3][1] = response.data.address
        this.setState({ contactUs: contactus })
        const followUs = { ...this.state.followUs }
        let items = [
          {
            src: images.footer.twitter,
            path: response.data.twitter
          },
          {
            src: images.footer.facebook,
            path: response.data.facebook
          },
          {
            src: images.footer.instagram,
            path: response.data.instagram
          },
          {
            src: images.footer.medium,
            path: response.data.medium
          }
        ]
        followUs.items = items
        this.setState({ followUs: followUs })
        }
    }).catch((error) => {
    
    });
    axios.get(Urls.institute.services).then(response => {
      if (response.data.length != 0 || response.data != null) {
        let fieldIndex = this.state.lists.findIndex(field => {
          return field.title === 'SERVICES'
        })
        const lists = [...this.state.lists]
        const services = []
        response.data.map((element, index) => {
          services.push({
            name: element.title,
            path: `/services/post/?id=${element.id}`,
          })
          lists[fieldIndex].items = services

        })
        this.setState({ items: lists })

      }
    }).catch(error=>{

    })
  }
  render(props) {
    return (
      <footer className={`${style.footer} bg-light position-relative d-flex justify-content-center align-items-center`}>
        <Container className="h-auto">
          <Row className="py-5 py-sm-0 text-center ">
            <Col className='py-3' xs="12" sm="6" md="6" lg='3'> <Column title={this.state.lists[0].title} list={this.state.lists[0].items} /></Col>
            <Col className='py-3' xs="12" sm="6" md="6" lg='3'><Column title={this.state.lists[1].title} list={this.state.lists[1].items} /></Col>
            <Col className='py-3' xs="12" sm="6" md="6" lg='3'><Column title={this.state.contactUs.title} list={this.state.contactUs.items} /></Col>
            <Col className='py-3' xs="12" sm="6" md="6" lg='3'><Column class="inline" title={this.state.followUs.title} list={this.state.followUs.items} /></Col>
          </Row>
        </Container>
      </footer>
    );
  }
}
export default footer
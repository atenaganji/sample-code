import React, {useState, Component} from 'react';
import style from './css/footer.module.scss'

const followusItems = (props) => {

    return (
        <li className={style.li}><a target="_blank" href={props.content.path}><img
            src={props.content.src}></img></a></li>
    );
}
export default followusItems;
import React, { useState, Component } from 'react';
import style from './css/footer.module.scss'
const Item = (props) => {
        return (
                <li className={`${style.li} text-dark`}><a className='text-dark' href={props.content.path}>{props.content.name}</a></li>
        );
}
export default Item
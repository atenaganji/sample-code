import React from 'react';
import style from './css/footer.module.scss'
const Item = (props) => {

    return (
        <li className={style.li}><span><img src={props.src}></img></span><span>{props.content}</span></li>
    );

}
export default Item
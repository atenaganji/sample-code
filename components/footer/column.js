import React, { useState, Component } from 'react';
import style from  './css/footer.module.scss'
import Item from './item'
import FollowUsItem from './followusItem'
import ContactUsItem from './contactitems'
class Column extends Component {

    constructor(props) {
        super(props);
    }


    items() {

        return (
            this.props.list.map((element, index) => {

                if (this.props.title == "CONTACT US") {

                    return (
                        <ContactUsItem key={index} content={element[1]} src={element[0]} />
                    )
                }
                if (this.props.title == "FOLLOW US") {
                    return (

                        <FollowUsItem key={index} content={element} />
                    )
                }
                else {
                    return (
                        <Item key={index} content={element} />
                    )
                }
                ;
            }))
    }

    render() {

        return (
            <ul className={`px-5 px-sm-3 px-md-5 px-lg-0 list-unstyled text-center text-md-left ${this.props.class}`}>
                <div className={`${style.heading} border-secondary mx-auto mx-md-0`}>{this.props.title} </div>
                {this.items()}
            </ul>
        );
    }
}

export default Column;
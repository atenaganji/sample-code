import React from 'react';
import { Component } from 'react';
import style from './css/lineTitle.module.scss'

class LineTitle extends Component{
constructor(props){
    super(props)
}
    render(){
        return(
             <span className={`${style.lineTitle} ${this.props.color} mb-5`} >{this.props.content}</span>
        )
    }
}
export default LineTitle;
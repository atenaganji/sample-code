import React, { Component } from 'react';
import Heading from './heading'
import Paragraph from './paragraph'
import LineTitle from './lineTitle'

class TextSection extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <div>
                <React.Fragment>
                    <LineTitle color={this.props.colorTitle} content={this.props.lineTitle} />
                    <Heading content={this.props.heading} color={this.props.colorHeading} />
                    <Paragraph color={this.props.colorParaph} content={this.props.paraph} />
                </ React.Fragment>
            </div>
        )
    }
}
export default TextSection
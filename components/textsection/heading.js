import React, { useState } from 'react';
import style from './css/heading.module.scss'

function Heading(props) {
    return (
        <h1 className={`${style.heading} ${props.color} mt-3`}>{props.content}</h1>
    )
}
export default Heading;
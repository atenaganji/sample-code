import React from 'react';
import style from './css/paragraph.module.scss'
function Paragraph(props) {
    return (
        <p className={`${style.p} ${props.color}`} dangerouslySetInnerHTML={{ __html: props.content }}></p>
    )
}
export default Paragraph;
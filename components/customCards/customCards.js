import React, { Component } from 'react';
import style from './customCards.module.scss'
import Link from 'next/link'

class CustomCards extends Component {
  constructor(props) {
    super(props)
  }
  render(props) {

    return (
      <div className={style.card} >
        <div className="img-container">
          <div className={style.shadowImg}>
            <img src={this.props.src} alt=""
              className={`${style.cardImg} card-img`} />
          </div>
        </div>
        <div className={style.cardImgOverlay}>
          <div className={style.icons}>
            <h4 className={style.title}>{this.props.overly_title}</h4>
            <p className={style.text} dangerouslySetInnerHTML={{ __html: (this.props.overly_des > 120) ? (this.props.overly_des.slice(0, 120)) : (this.props.overly_des) }}></p>
            <Link href={this.props.path}>
              <a type="button" className={`${style.moreInfo} tracking`}>more information</a>
            </Link>
          </div>
          <div className={style.title}>
            <h4>{this.props.title}</h4></div>
        </div>
      </div>

    )
  }
}
export default CustomCards
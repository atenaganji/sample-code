import images from '..'
import bg_1 from './bg-1.jpg'
import bg_2 from './bg-2.jpg'
import bg_3 from './bg-3.jpg'

const defaultImages = {
    bg_1,
    bg_2,
    bg_3
}
export default defaultImages
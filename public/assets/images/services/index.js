import air from './air.jpg'
import sea from './sea.jpg'
import road from './road.jpg'
import custom from './custom-bg.jpg'
import air_bg from './air-bg.jpg'
import sea_bg from './sea-bg.jpg'
import road_bg from './road-bg.jpg'
const services = {
    air,
    sea,
    road,
    custom,
    air_bg,
    sea_bg,
    road_bg
}
export default services
import item_1 from './shield-check.svg'
import item_2 from './credit-card.svg'
import item_3 from './box.svg'
import item_4 from './bag.svg'
import path_1 from './Path_1.png'
import path_2 from './Path_2.png'
import path_3 from './Path_3.png'
import path_4 from './Path_4.png'
import path_5 from './Path_5.png'

const values={
    item_1,
    item_2,
    item_3,
    item_4,
    path_1,
    path_2,
    path_3,
    path_4,
    path_5
}
export default values
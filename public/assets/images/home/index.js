import header from './header'
import values from './values'

const home={
    header,
    values
}
export default home
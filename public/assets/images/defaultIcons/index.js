import arrowDown from './arrowDown.svg'
import email_logo from './email-logo.svg'
 const defaultIcons = {
    arrowDown,
    email_logo
}
export default defaultIcons
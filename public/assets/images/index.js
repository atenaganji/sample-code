import footer from './footer'
import sliderIcons from './slideIcons'
import defaultImages from './defaultImages'
import home from './home'
import services from './services'
import defaultIcons from './defaultIcons'
import logo from './logo'
import contactUs from './contactUs'

const images = {
    footer,
    sliderIcons,
    defaultImages,
    home,
    services,
    defaultIcons,
    logo,
    contactUs
}
export default images
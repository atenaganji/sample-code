import leftBtn_1 from './leftBtn.svg'
import rightBtn_1 from './rightBtn.svg'
import leftBtn_2 from './leftBtn-2.svg'
import rightBtn_2 from './rightBtn-2.svg'
import leftBtn_3 from './leftbtn-3.svg'
import rightBtn_3 from './rightbtn-3.svg'
const slideIcons = {
    leftBtn_1,
    rightBtn_1,
    leftBtn_2,
    rightBtn_2,
    rightBtn_3,
    leftBtn_3
}
export default slideIcons
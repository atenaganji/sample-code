import facebook from './facebook.svg'
import instagram from './instagram.svg'
import mail from './mail.svg'
import map from './map.svg'
import medium from './medium.svg'
import phone from './phone.svg'
import twitter from './twitter.svg'
import whatsapp from './whatsapp.png'

const footer={
    facebook,
    instagram,
    mail,
    map,
    medium,
    phone,
    twitter,
    whatsapp
}
export default footer
import React, { Component, Suspense, useEffect } from 'react';
import { Router,Route, Switch, Redirect, Link, BrowserRouter, StaticRouter } from 'react-router-dom'
import { createMemoryHistory } from 'history';
import * as routes from './routes'

export default function routers() {
    const routObject = () => {
        let routers=[]
        debugger
        for(const[key,value] of Object.entries(routes.routes)){
            routers.push(<Route key={key} component={value.component} path={value.path} exact/>)
        }
        return (
   routers
        )
    }
    const history = createMemoryHistory();

    return (
        <Router history={history}> <Switch>{routObject()}</Switch></Router>
    )
} 
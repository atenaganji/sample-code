import * as actionTypes from '../actions/resetpass' 


const initialState = {
    token: '',
}
const authReducer=(state=initialState,action)=>{
    switch(action.type){
        case actionTypes.STORE_TOKEN_RESETPASS:
            localStorage.setItem('token-resetpass', action.value)
                       return{
            ...state,
            token:action.value,
                    }
        case actionTypes.EMPTY_TOKEN_RESETPASS:
            localStorage.removeItem('token-resetpass')
            return{
                ...state,
                token:''
            }
    }
 
    return state
}

export default authReducer;
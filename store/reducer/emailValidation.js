import * as actionTypes from '../actions/emailValidation' 


const initialState = {
    email: '',
}
const authReducer=(state=initialState,action)=>{
    switch(action.type){
        case actionTypes.STORE_EMAIL_VALIDATION:
            localStorage.setItem('emailvalid', action.value)
                       return{
            ...state,
            email:action.value,
                    }
        case actionTypes.EMPTY_EMAIL_VALIDATION:
            localStorage.removeItem('emailvalid')
            return{
                ...state,
                email:''
            }
    }
 
    return state
}

export default authReducer;
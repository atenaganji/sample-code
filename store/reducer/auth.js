import * as actionTypes from '../actions/auth'


const initialState = {
    token: null,
    authen: false,
    username: null,
    userfamily: null,
    useremail: null,
    error: null,
    loading: false
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        
        case actionTypes.AUTH_SUCCESS:
            localStorage.setItem('token', action.value)
            return {
                ...state,
                token: action.value,
                athen: true

            }

        case actionTypes.AUTH_REMOVE:
            localStorage.removeItem('token')
            return {
                ...state,
                token: null,
                authen: false,
            }

        default: return state
    }

}

/*const authStart = (state, action) => {
    return updateObject(state, { error: null, loading: true })
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        token: action.token,
        userId: 1,
        error: null,
        loading: false
    })
}
const authFail = (state, action) => {
    return updateObject(state, { error: action.error, loading: false })
}
const authLogout = (state, action) => {
    return updateObject(state, { token: null, userId: null })
}
/*const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action)
        case actionTypes.AUTH_FAIL: return authFail(state, action)
        case actionTypes.AUTH_LOGOUT: return authLogout(state, action)
        default: return state
    }
}*/

export default authReducer;
import {useRouter} from "next/router";

function withRouter(Component){
    return function WrappedComponent(props) {
      const router = useRouter();
      return <Component {...props} router={router} />;
    }
  }

export  default  withRouter


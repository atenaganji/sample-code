import React, { Component } from 'react';
import { Col, Row} from 'reactstrap';
import { Container } from 'reactstrap'
import images from './../../public/assets/images'
import NavBar from '../../components/navbar/navbaritems'
import Footer from '../../components/footer/footer'
import axios from '../../axios/axios';
import Link from 'next/link'
import * as Urls from '../../reqURL/url'
import style from './aboutUs.module.scss'

class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            support: {
                email: '',
                phone: '',
                whats: ''
            },

            post: {
                id: '',
                src: images.defaultImages.bg_1,
                date: '',
                title: 'About Us',
                caption: '',
                counter: ''
            },
        }
    }

    componentDidMount() {
        axios.get(Urls.home.socialnetworks).then((response) => {
            let support = {
                ...this.state.support
            }
            support.email = response.data.email
            support.phone = response.data.phone_number
            support.whats = response.data.whats_app_phone_number
            this.setState({ support: support })

        }).catch((error) => {
            console.log(error.response)
        });
        axios.get(Urls.institute.aboutus).then(res => {
            console.log(res)
            if (Object.keys(res.data).length !== 0) {
                let posts = {}
                posts.id = res.data.id
                posts.title = res.data.title
                posts.date = res.data.date
                posts.src = res.data.image
                posts.caption = `${res.data.description}`
                posts.counter = res.data.counter
                this.setState({ post: posts })
                console.log(this.state.posts)
            }
        })

    }

    render() {
        return (
            <section className={`${style.aboutUs} light-nav position-relative`}>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light' /></Row> </Container>
                <div>
                    <Container className='mt-5 pt-3  h-auto'>
                        <Row className='text-center mt-5 pt-5'>
                            <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                                <Link href='/' >
                                    <a className='text-muted'>&#10094; HOME</a>
                                    </Link>
                            </div>
                            <Col sm='12' md='12'>                <h1>
                                {this.state.post.title}
                            </h1>
                                <p className='text-danger font-weight-bold'>
                                    SKYMOVEXLOGISTICS
                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={style.img} src={this.state.post.src}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        {(this.state.post.caption === '') ?
                            <React.Fragment>
                                <Row >
                                    <Col className='mx-4'> <p>We are an independent freight forwarding and logistics company based in Turkey- Istanbul, ready to provide professional solutions for your shipment globally.</p>
                                        <br></br>
                                        <p>In Skymovex we support all our clients with a large range of professional, reliable and trustworthy logistic services but above all – we make sure there is no extra charge or time wasting for your business while working with us.</p>
                                        <br></br>
                                        <p>Our well experienced team manage any transport requirements you have, no matter how big or small your shipments are, making us the first choice for International Freight Forwarder and we fully support you and your business.</p></Col>

                                </Row>
                                <Row className='mx-4'>
                                    <Col sm='12'><p className='font-weight-bold'>The services available in Skymovex are:</p></Col>
                                    <Col sm='12'>
                                        <ul>
                                            <li>Export and impact logistics service</li>
                                            <li>Air/Sea/Road freight solutions- or a mix of them whatever suites your shipments</li>
                                            <li>Customs clearance</li>
                                            <li>Documents revieW and advice on the latest rules and technology </li>
                                            <li>Support on your business purchase and logistics</li>
                                            <li>Providing services for dangerous goods via our partner’s companies</li>
                                        </ul>
                                    </Col>
                                    <Col className='pt-3' sm='12'>
                                        <p>
                                            We will offer the best solution suits your business which could be one of above freight or a mix of them based on your time, budget and specifications. Tell us your origin and destination and we will manage the rest for you!!
            </p>
                                        <p>
                                            In addition to above services we have specialist in our team who could provide over flight and landing permission for the commercial and cargo flights.
            </p>
                                        <p>
                                            To get the latest freight rate please contact us (mail) and (WhatsApp)
            </p>
                                        <p>
                                            Whatever your requirements, Skymovex will offer freight rates that reflect the fast and reliable service could be achieved.
            </p>
                                        <br></br>
                                        <p className='mb-0'>Contact us:</p>
                                        <p className="mb-0">Mail: <span className="text-dark font-weight-bold">{this.state.support.email}</span></p>
                                        <p className="mb-0" >Mob: <span className="text-dark font-weight-bold">{this.state.support.phone}</span> </p>
                                        <p className="mb-0">WhatsApp: <span className="text-dark font-weight-bold">{this.state.support.whats}</span></p>
                                    </Col>
                                    <Col sm='12 mt-4'>
                                        <p className='font-weight-bold'>Who We Are:</p>
                                        <p>Skymovex is a leading independent logistic provider in the shipping and forwarding industry based in Turkey. We provide freight forwarding services all around the globe. Our reliable team make the process of moving your goods simple, stress free and efficient.</p>
                                        <p className='font-weight-bold'>In Skymovex we support our customers by:</p>
                                    </Col>
                                    <Col sm='12'>
                                        <ul>
                                            <li>Import/ Export and customs clearance </li>
                                            <li>Air/ Sea/ Road Freight or a mix service available based on your cargo and needs specification</li>
                                            <li>A cost effective and reliable service</li>
                                            <li>Door to door, FCA, EXW, FOB, etc. services available </li>
                                            <li>Customs clearance</li>
                                            <li>The latest knowledge of the rules and technology</li>
                                            <li>Free consulting</li>
                                            <li>Ideal choice for businesses of all sizes</li>
                                        </ul>
                                    </Col>
                                </Row>
                            </React.Fragment>
                            :
                            <Row >
                                <Col className='mx-4' dangerouslySetInnerHTML={{ __html: this.state.post.caption }}></Col>
                            </Row>
                        }
                    </Container>
                </div>
                <div className='py mt-5'><Footer /></div>
            </section>
        )
    }

}
export default Index
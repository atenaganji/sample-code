import React, {Component} from 'react';
import {Col, Row, Container} from 'reactstrap';
import Heading from '../../../components/textsection/heading'
import Link from 'next/link'
import CustomCards from "../../../components/customCards/customCards"
import NavBar from '../../../components/navbar/navbaritems'
import Footer from '../../../components/footer/footer'
import styles from './../services.module.scss'
import images from './../../../public/assets/images/services'
import LineTitle from '../../../components/textsection/lineTitle'

class Custom extends Component {

    constructor(props) {

        super(props)
        this.state = {
            service: '',
            services: [
                {
                    name: '',
                    title: '',
                    image: ""

                }
            ],
            posts: [
                {
                    id: '',
                    src: images.air,
                    date: 'September 3,2020',
                    title: 'Air Freight',
                    path: '/services/air',
                    caption: `Skymovex offers you a smooth shipment between your partners around the world in a cost effective manner.
                  A door to door service, we support your shipment until it will be clear from customs and get in your hands
                  `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: images.road,
                    path: '/services/road',
                    date: 'September 3,2020',
                    caption: `Faster than sea freight and cheaper than air freight.
                  Road Freight is one of the secure, cost effective and flexible method of cargo transportation all the time
                  `,
                    title: 'Road Freight',
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: images.sea,
                    path: '/services/sea',
                    date: 'September 3,2020',
                    title: 'Sea Freight',
                    caption: `Skymovex suggested Sea Freight Services if you have a large volumes of cargo. It’s the best method of transporting the huge cargo in a cost effective ways especially when your shipment is not time sensitive. `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },


            ]
        }
    }

    items = () => {
        let items = []
        if (this.state.posts.length > 4)
            items = this.state.posts.slice(0, 4)
        else {
            items = this.state.posts
        }
        return (
            items.map((element, index) => {
                return (
                    <Col key={index} className='py-2' sm='12' md='4'>
                        <CustomCards path={element.path} src={element.src} title={element.title}
                                     overly_title={element.title} overly_des={element.caption}></CustomCards>
                    </Col>)
            })
        )


    }

    render(props) {
        return (
            <section className={`${styles.services} light-nav position-relative`}>

                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>

                <div>
                    <Container className='mt-5 pt-3 h-auto'>

                        <Row className='text-center mt-md-5 pt-5'>
                            <div className='mt-5 pl-2 pl-md-0 pt-5'>
                                <Link href='/'>
                                    <a className='text-muted'>&#10094; HOME</a>
                                </Link>
                            </div>
                            <Col sm='12' md='12'>
                                <h1>
                                    Customs Clearance
                                </h1>
                                <h1>
                                    Custom Logistics, Affordable
                                </h1>
                                <p className='text-danger font-weight-bold'>
                                    SKYMOVEXLOGISTICS
                                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={styles.img} src={images.custom}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='12' className=''>
                                <h5>Customs Clearance Facilities</h5>
                                <p>As an importer with huge sizes of shipments or owner of a small business with a small
                                    cargo, you will benefit to use our custom clearance services to clear your goods in
                                    a timely manner. Less time means less costs! Time is precious.</p>
                                <p>Make sure to ask our team for latest customs information to make sure your shipment
                                    reach to you smooth and safe.</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='12'>
                                <div className="py-4">
                                    <LineTitle color="text-danger" content="SKYMOVEXLOGISTICS"/>
                                    <Heading color="text-dark" content="See also our other servise"/>
                                </div>
                            </Col>
                            {this.items()}
                        </Row>
                    </Container>
                </div>
                <div>
                    <Footer/>
                </div>
            </section>
        )
    }
}

export default Custom
import React, {Component} from 'react';
import {Col, Row, Container} from 'reactstrap';
import Heading from './../../../components/textsection/heading'
import NavBar from './../../../components/navbar/navbaritems'
import Footer from './../../../components/footer/footer'
import styles from './singlepost.module.scss';
import LineTitle from './../../../components/textsection/lineTitle'
import axios from './../../../axios/axios'
import Link from 'next/link'
import Slick from 'react-slick'
import images from './../../../public/assets/images'
import CustomCard from './../../../components/customCards/customCards'
import * as Urls from './../../../reqURL/url'

class Post extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: '',
            service: '',
            services: [
                {
                    id: '',
                    src: '',
                    date: 'September 3,2020',
                    title: 'Air Freight',
                    path: '/services/air',
                    caption: `Skymovex offers you a smooth shipment between your partners around the world in a cost effective manner.
                            A door to door service, we support your shipment until it will be clear from customs and get in your hands
                            `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: '',
                    path: '/services/road',
                    date: 'September 3,2020',
                    caption: `Faster than sea freight and cheaper than air freight.
                            Road Freight is one of the secure, cost effective and flexible method of cargo transportation all the time
                            `,
                    title: 'Road Freight',
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: '',
                    path: '/services/sea',
                    date: 'September 3,2020',
                    title: 'Sea Freight',
                    caption: `Skymovex suggested Sea Freight Services if you have a large volumes of cargo. It’s the best method of transporting the huge cargo in a cost effective ways especially when your shipment is not time sensitive. `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
            ],
            post: {
                id: '',
                src: '',
                date: '',
                title: 'Lorem ipsum dolor sit amet',
                caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                counter: ''
            },
            slideItems: {
                sm: 1,
                md: 2,
                lg: 3
            }
        }
    }

    componentDidMount() {
        const query = new URLSearchParams(window.location.search)
        let id = query.entries().next().value[1]
        this.setState({id: id})
        axios.get(Urls.institute.services + id).then(res => {
            let posts = {}
            posts.id = res.data.id
            posts.title = res.data.title
            posts.date = new Date(res.data.date).toLocaleDateString()
            posts.src = res.data.image
            posts.caption = `${res.data.description}`
            posts.counter = res.data.counter
            this.setState({post: posts})
            console.log(this.state.posts)
        });

        axios.get(Urls.institute.services).then(response => {
            if (response.data.length != 0) {
                let services = []
                response.data.map((element, index) => {
                    services.push({
                        id: element.id,
                        title: element.title,
                        date: element.date,
                        src: element.image,
                        path: `/services/post/?id=${element.id}`,
                        caption: element.description,
                        counter: element.counter,
                    })
                    this.setState({services: services})
                })
                if (services.length = 2) {
                    return {...this.state.slideItems, lg: 2}
                }
                if (services.length = 1) {
                    return {...this.state.slideItems, md: 1, lg: 1}
                }
            }
        })
    }

    items = () => {
        let items = []
        items = this.state.services
        return (
            items.map((element, index) => {
                return (
                    <div key={index}>
                        <CustomCard path={element.path} src={element.src} title={element.title}
                                    overly_title={element.title} overly_des={element.caption}></CustomCard>
                    </div>
                )
            })
        )
    }

    SampleNextArrow = (props) => {
        const {className, style, onClick} = props;
        return (
            <div
                className={styles.nextBtn}
                style={{...style, display: "block", right: '78px', zIndex: '2'}}
                onClick={onClick}>
                <span><img type="button" className="mr-2" src={images.sliderIcons.rightBtn_2}/></span>
            </div>
        );
    }
    SamplePrevArrow = (props) => {
        const {className, style, onClick} = props;
        return (
            <div
                className={styles.prevBtn}
                style={{...style, display: "block", right: '134px', zIndex: '2'}}
                onClick={onClick}>
                <span><img type="button" className="mr-2" src={images.sliderIcons.leftBtn_2}/></span>
            </div>
        );
    }

    render(props) {
        const settings = {
            dots: true,
            speed: 500,
            initialSlide: 0,
            infinite: false,
            slidesToShow: this.state.slideItems.lg,
            slidesToScroll: this.state.slideItems.lg,
            prevArrow: <this.SamplePrevArrow/>,
            nextArrow: <this.SampleNextArrow/>,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: this.state.slideItems.lg,
                        slidesToScroll: this.state.slideItems.lg,
                        dots: true
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: this.state.slideItems.md,
                        slidesToScroll: this.state.slideItems.md,
                        dots: true
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        dots: true,
                        slidesToShow: this.state.slideItems.sm,
                        slidesToScroll: this.state.slideItems.sm
                    }
                }
            ]
        }
        return (
            <section className={`${styles.post} single-service light-nav position-relative`}>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>
                <div>
                    <Container className='mt-5 pt-3 px-5 h-auto'>
                        <Row className='text-center mt-md-5 pt-5'>
                            <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                                <Link className='text-muted' href='/'>&#10094; HOME</Link>
                            </div>
                            <Col sm='12' md='12'>
                                <h1>
                                    {(this.state.post.title) ? (this.state.post.title) : null}
                                </h1>
                                <p className='text-danger font-weight-bold'>
                                    SKYMOVEXLOGISTICS
                                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={styles.img} src={this.state.post.src}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='mx-4' dangerouslySetInnerHTML={{__html: this.state.post.caption}}></Col>
                        </Row>
                        <Row className='position-relative my-5'>
                            <Col sm='12 my-5'>
                                <div className="py-4">
                                    <LineTitle color="text-danger" content="SKYMOVEXLOGISTICS"/>
                                    <Heading color="text-dark" content="See also our other servise"/>
                                </div>
                            </Col>
                            <Col className='' sm='12'>
                                <Slick style={{width: '100%'}} {...settings}>
                                    {
                                        this.items()
                                    }
                                </Slick>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className='mt-5 py-5'>
                    <Footer/>
                </div>
            </section>
        )
    }
}

export default Post
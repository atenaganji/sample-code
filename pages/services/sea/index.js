import React, {Component} from 'react';
import {Col, Row, Container} from 'reactstrap';
import Heading from '../../../components/textsection/heading'
import Link from 'next/link'
import CustomCards from "../../../components/customCards/customCards"
import NavBar from '../../../components/navbar/navbaritems'
import Footer from '../../../components/footer/footer'
import styles from './../services.module.scss'
import images from './../../../public/assets/images/services'
import LineTitle from '../../../components/textsection/lineTitle'

class Sea extends Component {

    constructor(props) {

        super(props)
        this.state = {
            service: '',
            services: [
                {
                    name: '',
                    title: '',
                    image: ""

                }
            ],
            posts: [
                {
                    id: '',
                    src: images.air,
                    date: 'September 3,2020',
                    title: 'Air Freight',
                    path: '/services/air',
                    caption: `Skymovex offers you a smooth shipment between your partners around the world in a cost effective manner.
                  A door to door service, we support your shipment until it will be clear from customs and get in your hands
                  `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: images.road,
                    path: '/services/road',
                    date: 'September 3,2020',
                    caption: `Faster than sea freight and cheaper than air freight.
                  Road Freight is one of the secure, cost effective and flexible method of cargo transportation all the time
                  `,
                    title: 'Road Freight',
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },

                {
                    id: '',
                    src: images.custom,
                    path: '/services/custom',
                    date: 'September 3,2020',
                    title: 'Custom clearance',
                    caption: `As an importer with huge sizes of shipments or owner of a small business with a small cargo, you will benefit to use our custom clearance services to clear your goods in a timely manner. Less time means less costs! Time is precious.`,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },


            ]
        }
    }

    componentDidMount() {
    }

    items = () => {
        let items = []
        if (this.state.posts.length > 4)
            items = this.state.posts.slice(0, 4)
        else {
            items = this.state.posts
        }
        return (
            items.map((element, index) => {
                return (
                    <Col key={index} className='py-2' sm='12' md='4'>
                        <CustomCards path={element.path} src={element.src} title={element.title}
                                     overly_title={element.title} overly_des={element.caption}></CustomCards>
                    </Col>)
            })
        )

    }

    render(props) {
        return (
            <section className={`${styles.services} light-nav position-relative`}>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>
                <div>
                    <Container className='mt-md-5 pt-3 h-auto'>
                        <Row className='text-center mt-5 pt-5'>
                            <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                                <Link href='/'>
                                    <a className='text-muted'>&#10094; HOME</a>
                                </Link>
                            </div>
                            <Col sm='12' md='12'>
                                <h1>
                                    Sea Freight
                                </h1>
                                <h1>
                                    Large volumes of cargo,Unlimited
                                </h1>
                                <p className='text-danger font-weight-bold'>
                                    SKYMOVEXLOGISTICS
                                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={styles.img} src={images.sea_bg}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='mx-4'><p>If you need more details about Sea Freight service, our team of
                                well trained specialists can give you the answers to all your questions and help you
                                with the best solutions for your needs.</p>
                                <br></br>
                                <p>Skymovex suggested Sea Freight Services if you have a large volumes of cargo. It’s
                                    the best method of transporting the huge cargo in a cost effective ways especially
                                    when your shipment is not time sensitive. However we ship all sizes of cargo by sea
                                    including full or less container load.</p>
                                <br></br>
                                <p>In Skymovex we can provide you a smooth and reliable Sea services to deliver your
                                    goods to your destination port. It’s a cost effective way of shipping goods.</p>
                            </Col>

                        </Row>
                        <Row className='mx-4'>
                            <Col sm='12'><p className='font-weight-bold'>SkyMoveX Features:</p></Col>
                            <Col sm='12'>Skymovex would like to offer you Air Freight Services including processing,
                                transit, customs clearance and delivery to your door. </Col>
                            <Col sm='12'>
                                <ul>
                                    <li>Import/ Export of all sizes of packages</li>
                                    <li>FCL/LCL - FCL (Full container loads) both import and export containers
                                        LCL (Less container loads) both import and export shipments
                                    </li>
                                    <li>clearance process</li>
                                    <li>Import and export documentation preparation</li>
                                    <li>Fiscal clearance</li>
                                    <li>Warehouse transit</li>
                                    <li>Custom and tariff consultancy</li>
                                </ul>
                            </Col>
                            <Col sm='12 mt-4'>
                                <p className='font-weight-bold'>Our plans:</p>
                            </Col>
                            <Col sm='12'>
                                <ul>
                                    <li> We are working with some of the largest shipping lines which can carry whether
                                        lass than container load (LCL) or full container load (FCL)
                                    </li>
                                    <li> We can offer a mix of the Sea/Air/ Road freight for a more convenience shipment
                                        if it suits your shipment
                                    </li>
                                    <li>If you are a buyers in another country we could support you through our
                                        worldwide partners for a cost effective and time efficient shipping. From
                                        document preparation through shipment, custom formalities until delivery to your
                                        hands
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                        <Row className=''>
                            <Col sm='12'>
                                <div className="py-4">
                                    <LineTitle color="text-danger" content="SKYMOVEXLOGISTICS"/>
                                    <Heading color="text-dark" content="See also our other servise"/>
                                </div>
                            </Col>
                            {this.items()}
                        </Row>
                    </Container>
                </div>
                <div>
                    <Footer/>
                </div>

            </section>
        )
    }
}

export default Sea
import React, {Component} from 'react';
import {Col, Row, Container} from 'reactstrap';
import Link from 'next/link'
import Heading from '../../../components/textsection/heading'
import NavBar from '../../../components/navbar/navbaritems'
import CustomCards from "../../../components/customCards/customCards"
import Footer from '../../../components/footer/footer'
import styles from  './../services.module.scss';
import LineTitle from '../../../components/textsection/lineTitle'
import images from '../../../public/assets/images/services'

class Air extends Component {

    constructor(props) {

        super(props)
        this.state = {
            service: '',
            services: [
                {
                    name: 'air',
                    title: '',
                    image: ""
                }
            ],
            posts: [
                {
                    id: '',
                    src: images.road,
                    path: '/services/road',
                    date: 'September 3,2020',
                    caption: `Faster than sea freight and cheaper than air freight.
                  Road Freight is one of the secure, cost effective and flexible method of cargo transportation all the time
                  `,
                    title: 'Road Freight',
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: images.sea,
                    path: '/services/sea',
                    date: 'September 3,2020',
                    title: 'Sea Freight',
                    caption: `Skymovex suggested Sea Freight Services if you have a large volumes of cargo. It’s the best method of transporting the huge cargo in a cost effective ways especially when your shipment is not time sensitive. `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: images.custom,
                    path: '/services/custom',
                    date: 'September 3,2020',
                    title: 'Custom clearance',
                    caption: `As an importer with huge sizes of shipments or owner of a small business with a small cargo, you will benefit to use our custom clearance services to clear your goods in a timely manner. Less time means less costs! Time is precious.`,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },


            ]
        }
    }

    componentDidMount() {
        /* const query=new URLSearchParams(this.props.location.search)
         alert(query.entries().next().value[1])
         for (const param of query.entries()) {
             alert(query.entries()[0] )  
         }*/
    }


    items = () => {
        let items = []
        if (this.state.posts.length > 4)
            items = this.state.posts.slice(0, 4)
        else {
            items = this.state.posts
        }
        return (
            items.map((element, index) => {
                return (
                    <Col key={index} className='py-2' sm='12' md='4'>
                        <CustomCards path={element.path} src={element.src} title={element.title}
                                     overly_title={element.title} overly_des={element.caption}></CustomCards>
                    </Col>)
            })
        )


    }


    render(props) {
        return (
            <section className={`${styles.services} light-nav position-relative`}>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>
                <div>
                    <Container className='mt-md-5 pt-3  h-auto'>
                        <Row className='text-center mt-5 pt-5'>
                            <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                                <Link href='/'>
                                    <a className='text-muted'>&#10094; HOME</a>
                                </Link>
                            </div>
                            <Col sm='12' md='12'>
                                <h1>
                                    Air Freight
                                </h1>
                                <h1>
                                    Efficient, Fast and cost Effective
                                </h1>
                                <p className='text-danger font-weight-bold'>
                                    SKYMOVEXLOGISTICS
                                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={styles.img} src={images.air_bg}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='mx-4'><p>If you need more details about Air Freight our dedicated team for
                                domestic or international road freight will help you and give you the best solutions for
                                your business</p>
                                <br></br>
                                <p>Looking for an efficient, fast and cost effective Air Freight service?</p>
                                <br></br>
                                <p>In a constantly changing and competitive logistic market, Air freight is still one of
                                    the best choices for a flexible, fast and cost effective shipment
                                    transportation.</p></Col>

                        </Row>
                        <Row className='mx-4'>
                            <Col sm='12'><p className='font-weight-bold'>SkyMoveX Features:</p></Col>
                            <Col sm='12'>Skymovex would like to offer you Air Freight Services including processing,
                                transit, customs clearance and delivery to your door. </Col>
                            <Col sm='12'>
                                <ul>
                                    <li>Import/ Export clearance process</li>
                                    <li>Daily efficient Air Freight ( we track your cargo all the time)</li>
                                    <li>Warehouse transit</li>
                                    <li>Customs clearance service</li>
                                    <li>Tariff consultancy</li>

                                </ul>
                            </Col>

                            <Col sm='12 mt-4'>
                                <p className='font-weight-bold'>Why you should use SkyMoveX:</p>
                                <p>Skymovex would like to offer you Air Freight Services including processing, transit,
                                    customs clearance and delivery to your door. </p>
                            </Col>
                            <Col sm='12'>
                                <ul>
                                    <li>We could carry your all sizes of cargo to your final destinations with the
                                        airline services
                                    </li>
                                    <li>We guarantee to offer you the best rates and services</li>
                                    <li>We ensuring you that all your goods carried safe, on time and cost effective. We
                                        balance between the cost, time and your special requirements to carry your goods
                                        effective and efficient to any destinations using our partners and
                                        subcontractors around the world
                                    </li>
                                    <li>To offer the clients the best air freight services, we are working with the best
                                        air carries in the region and our global network of staff, partners and
                                        subcontractors
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                        <Row className=''>
                            <Col sm='12'>
                                <div className="py-4">
                                    <LineTitle color="text-danger" content="SKYMOVEXLOGISTICS"/>
                                    <Heading color="text-dark" content="See also our other servise"/>
                                </div>
                            </Col>
                            {this.items()}
                        </Row>
                    </Container>
                </div>
                <div><Footer/></div>
            </section>
        )
    }
}

export default Air
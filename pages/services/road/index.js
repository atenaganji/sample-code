import React, {Component} from 'react';
import {Col, Row, Container} from 'reactstrap';
import Heading from '../../../components/textsection/heading'
import Link from 'next/link'
import CustomCards from "../../../components/customCards/customCards"
import NavBar from '../../../components/navbar/navbaritems'
import Footer from '../../../components/footer/footer'
import styles from './../services.module.scss'
import images from './../../../public/assets/images/services'
import LineTitle from '../../../components/textsection/lineTitle'

class Road extends Component {
    constructor(props) {

        super(props)
        this.state = {
            service: '',
            services: [
                {
                    name: '',
                    title: '',
                    image: ""

                }
            ],
            posts: [
                {
                    id: '',
                    src: images.air,
                    date: 'September 3,2020',
                    title: 'Air Freight',
                    path: '/services/air',
                    caption: `Skymovex offers you a smooth shipment between your partners around the world in a cost effective manner.
                  A door to door service, we support your shipment until it will be clear from customs and get in your hands
                  `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },

                {
                    id: '',
                    src: images.sea,
                    path: '/services/sea',
                    date: 'September 3,2020',
                    title: 'Sea Freight',
                    caption: `Skymovex suggested Sea Freight Services if you have a large volumes of cargo. It’s the best method of transporting the huge cargo in a cost effective ways especially when your shipment is not time sensitive. `,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: '',
                    src: images.custom,
                    path: '/services/custom',
                    date: 'September 3,2020',
                    title: 'Custom clearance',
                    caption: `As an importer with huge sizes of shipments or owner of a small business with a small cargo, you will benefit to use our custom clearance services to clear your goods in a timely manner. Less time means less costs! Time is precious.`,
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
            ]
        }
    }

    componentDidMount() {
        window.screenY = 0
    }

    items = () => {
        let items = []
        if (this.state.posts.length > 4)
            items = this.state.posts.slice(0, 4)
        else {
            items = this.state.posts
        }
        return (
            items.map((element, index) => {
                return (
                    <Col key={index} className='py-2' sm='12' md='4'>
                        <CustomCards path={element.path} src={element.src} title={element.title}
                                     overly_title={element.title} overly_des={element.caption}></CustomCards>
                    </Col>)
            })
        )


    }


    render(props) {
        return (
            <section className={`${styles.services} light-nav position-relative`}>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>
                <div>
                    <Container className='mt-5 pt-3  h-auto'>
                        <Row className='text-center mt-md-5 pt-5'>
                            <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                                <Link href='/'>
                                    <a className='text-muted'>&#10094; HOME</a>
                                </Link>
                            </div>
                            <Col sm='12' md='12'>
                                <h1>
                                    Road Freight
                                </h1>
                                <h1>
                                    Fast,reliable Time and Cost
                                </h1>
                                <p className='text-danger font-weight-bold'>
                                    SKYMOVEXLOGISTICS
                                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={styles.img} src={images.road_bg}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='12' className=''><p>For any questions and details you can always rely on our team
                                to give you the best answers and solutions.</p>

                                <p>Faster than sea freight and cheaper than air freight.</p>

                                <p>Road Freight is one of the secure, cost effective and flexible method of cargo
                                    transportation all the time</p>
                                <p>It covers all the needs of the customers in a reliable time and cost</p>
                            </Col>
                            <Col sm='12' className=''>
                                <p>For more information and to get a competitive price contact us</p>
                                <p>We could support you for Road Freight and custom clearance whether you have a small
                                    parcel or a full truck to be delivered quickly and cost effective. </p>
                            </Col>
                        </Row>

                        <Row className=''>
                            <Col sm='12'>
                                <div className="py-4">
                                    <LineTitle color="text-danger" content="SKYMOVEXLOGISTICS"/>
                                    <Heading color="text-dark" content="See also our other servise"/>
                                </div>
                            </Col>
                            {this.items()}
                        </Row>
                    </Container>
                </div>
                <div>
                    <Footer/>
                </div>
            </section>
        )
    }
}

export default Road
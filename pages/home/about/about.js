import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import TextSection from './../../../components/textsection/textsection'
import Slick from 'react-slick'
import Wrapper from '../../../layouts/wrapper/wrapper';
import styles from './about.module.scss'
import images from './../../../public/assets/images'
import axios from './../../../axios/axios'
import * as Urls from './../../../reqURL/url'

class About extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: 'About SkyMoveX',
            paraph: "We are an independent freight forwarding and logistics company based in Turkey- Istanbul, ready to provide professional solutions for your shipment globally. In Skymovex we support all our clients with a large range of professional, reliable and trustworthy logistic services but above all – we make sure there is no extra charge or time wasting for your business while working with us.  Our well experienced team manage any transport requirements you have, no matter how big or small your shipments are, making us the first choice for International Freight Forwarder and we fully support you and your business.",
            posts: [
                {
                    src: images.defaultImages.bg_1
                },
                {
                    src: images.defaultImages.bg_2
                },
                {
                    src: images.defaultImages.bg_3
                },
            ]

        }
    }


    items = () => {
        let items = []
        items = this.state.posts
        return (
            items.map((element, index) => {
                return (
                    <div key={index} className={styles.slideItems}><img className="img" src={element.src}/></div>
                )
            })
        )
    }

    componentDidMount() {
        axios.get(Urls.home.aboutus).then(response => {
            if (Object.keys(response.data).length !== 0) {
                let posts = []
                this.setState({title: response.data.title})
                this.setState({paraph: response.data.description})
                if (response.data.pictures.length !== 0) {
                    response.data.pictures.map((element, index) => {
                        posts.push(
                            {
                                src: element.image
                            }
                        )
                        this.setState({posts: posts})
                    })
                }
            }
        }).catch(error=>{

        })
    }

    SampleNextArrow = (props) => {
        const {className, style, onClick} = props;
        return (
            <div
                className={styles.nextBtn}
                style={{...style, display: "block", left: '0', zIndex: '2'}}
                onClick={onClick}>
                <span><img type="button" className="" src={images.sliderIcons.rightBtn_1}/></span>
            </div>


        );
    }
    SamplePrevArrow = (props) => {
        const {className, style, onClick} = props;
        return (
            <div
                className={styles.prevBtn}
                style={{...style, display: "block", left: '-50px', zIndex: '2'}}
                onClick={onClick}>
                <span><img type="button" className="" src={images.sliderIcons.leftBtn_1}/></span>
            </div>
        );
    }

    render() {
        const settings = {
            dots: true,
            className:"slickBorder",
            infinite: false,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 0,
            prevArrow: <this.SamplePrevArrow/>,
            nextArrow: <this.SampleNextArrow/>,
        }

        return (
            <section className={`position-relative overflow-hidden ${styles.about}`}>
                <Container fluid>
                    <Wrapper>
                        <Row>
                            <Col xs="12" md="6" className="pt-5 ">
                                <TextSection colorTitle="text-danger" lineTitle="SKYYMOVEX LOGISTICS"
                                             heading={this.state.title} colorHeading="text-dark" colorParaph="text-dark"
                                             paraph={this.state.paraph}/>
                            </Col>
                            <Col xs={{size: 10, offset: 2}} md={{size: 6, offset: 0}} className="px-0 ">
                                <Row className="my-5">
                                    <Slick style={{width: '100%'}} {...settings}>
                                        {this.items()}
                                    </Slick>
                                </Row>
                            </Col>
                        </Row>
                    </Wrapper>
                </Container>
            </section>
        )
    }
}

export default About
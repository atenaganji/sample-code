import React, { Component } from 'react';
import { Container, Row} from 'reactstrap'
import LineTitle from './../../../components/textsection/lineTitle'
import Heading from "./../../../components/textsection/heading"
import images from './../../../public/assets/images'
import style from './values.module.scss'

class Values extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <section className={`${style.values} position-relative pt-5  align-items-center bg-light`}>
                <Container fluid className="h-auto">
                    <Row className='d-block'>
                        <div className={`${style.valueContent} pl-0 pl-lg-0 w-100`}>
                            <div className={`${style.path_1} ${style.path}`}>
                                <img src={images.home.values.path_1}></img>
                            </div>
                            <div className={`${style.path_2} ${style.path}`}>
                                <img src={images.home.values.path_2}></img>
                            </div>
                            <div className={`${style.path_3} ${style.path}`}>
                                <img src={images.home.values.path_3}></img>
                            </div>
                            <div className={`${style.path_4} ${style.path}`}>
                                <img src={images.home.values.path_4}></img>
                            </div>
                            <div className={`${style.path_5} ${style.path}`}>
                                <img src={images.home.values.path_5}></img>
                            </div>
                            <div xs="12" md="6" className={`${style.title1} text-left my-4 my-sm-0 pl-3 pl-md-0`}>
                                <LineTitle className="container" color="text-danger" content="SKYMOVEX LOGISTICS" />
                                <Heading color="text-dark" content="Our Values" />
                            </div>
                            <div className={`${style.item1Img} ${style.valuesIcons} shadow`}>
                                <img src={images.home.values.item_1}></img>
                            </div>
                            <div className={`${style.item1Txt} ${style.txt}`}>
                                <h3>Trustworthy</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
                            </div>
                            <div className={`${style.item2Img} ${style.valuesIcons} shadow`}>
                                <img src={images.home.values.item_2}></img>
                            </div>
                            <div className={`${style.item2Txt} ${style.txt}`}>
                                <h3>Time and cost efficiency</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
                            </div>
                            <div className={`${style.item3Img} ${style.valuesIcons} shadow`}>
                                <img src={images.home.values.item_3}></img>
                            </div>
                            <div className={`${style.item3Txt} ${style.txt}`}>
                                <h3>Service Excellency</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
                            </div>
                            <div className={`${style.item4Img} ${style.valuesIcons} shadow`}>
                                <img src={images.home.values.item_4}></img>
                            </div>
                            <div className={`${style.item4Txt} ${style.txt}`}>
                                <h3>Collaboration</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</p>
                            </div>
                        </div>
                    </Row>
                </Container>
            </section>
        )
    }
}
export default Values
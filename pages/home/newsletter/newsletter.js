import React, {Component } from 'react';
import { Container,Form} from 'reactstrap';
import style from './newsletter.module.scss'
import axios from './../../../axios/axios';
import Toast from './../../../components/alertToastify/alert'
import * as Urls from './../../../reqURL/url'
class NewsLetter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text: 'To receive the latest news and information,enter your email to subscribe to our newsletter ',
            email: ''
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();

        let data = {
            'email': this.state.email,
        }

        axios.post(Urls.home.newsletter, data).then(response => {
            document.getElementById('newsletterInput').value = '';
            Toast('success', 'You have successfully subscribed to the newsletter', 'bottom-right', '5000')

            console.log(response)
        }).catch(error => {
            if (error.response !== undefined) {
                console.log(error.response)
                if (/^5\d{2}$/.test(error.response.status.toString())) {
                    Toast('error', 'Server connection error!', 'bottom-right', '5000')
                }
                else if (error.response.status == 400) {
                    Toast('error', 'This email already exists!', 'bottom-right', '5000')
                }
                else {
                    Toast('error', 'Server connection error!', 'bottom-right', '5000')
                }
            }
            else {

                Toast('error', 'Server connection error!', 'bottom-right', '5000')
            }
        })
    };

    render() {
        return (
            <section>
                <Container className="d-flex flex-column">
                    <div className={style.newsletter}>
                        <h4>{this.state.text}</h4>
                        <Form className={style.emailNewsetter} onSubmit={this.handleSubmit}>
                            <div className="input-group d-flex flex-nowrap shadow bg-white badge-pill pr-0 h-100 shadow ">
                                <div className="w-50 p-2">
                                    <input id='newsletterInput' type="email" className="border-0 w-100 h-100 pl-3" onChange={(event) => {
                                        this.setState({ email: event.target.value })
                                    }} placeholder="Email"></input>
                                </div>
                                <div className="d-flex align-items-center pl-1 w-50">
                                    <button type='submit' className="btn btn-danger w-100 h-100 badge-pill">NewsLetter</button>
                                </div>
                            </div>
                        </Form>
                    </div>
                </Container>

            </section>
        )


    }
}
export default NewsLetter
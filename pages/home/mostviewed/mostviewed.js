import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Wrapper from './../../../layouts/wrapper/wrapper'
import LineTitle from './../../../components/textsection/lineTitle'
import Heading from "./../../../components/textsection/heading"
import images from './../../../public/assets/images'
import axios from './../../../axios/axios'
import {
  Card, CardImg, CardText, CardBody,
  CardTitle
} from 'reactstrap';
import Slick from 'react-slick'
import styles from './mostviewed.module.scss'
import * as Urls from './../../../reqURL/url'

class MostViewed extends Component {

  constructor(props) {
    super(props)
    this.state = {
      title: "Most viewed",
      posts: [
        {
          id: '',
          src: images.defaultImages.bg_1,
          date: 'September 3,2020',
          title: 'Lorem ipsum dolor sit amet',
          caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.defaultImages.bg_2,
          date: 'September 3,2020',
          title: 'Lorem ipsum dolor sit amet',
          caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.defaultImages.bg_3,
          date: 'September 3,2020',
          title: 'Lorem ipsum dolor sit amet',
          caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.defaultImages.bg_1,
          date: 'September 3,2020',
          title: 'Lorem ipsum dolor sit amet',
          caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.defaultImages.bg_2,
          date: 'September 3,2020',
          title: 'Lorem ipsum dolor sit amet',
          caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
      ],
      slideItems: {
        sm: 1,
        md: 2,
        lg: 3
      }
    }
  }
  componentDidMount() {
    axios.get(Urls.institute.mostviewed).then(response => {
       if (response.data.length != 0) {
        let posts = []
        response.data.map((element, index) => {
          posts.push({
            id: element.id,
            title: element.title,
            date: new Date(element.date).toLocaleDateString(),
            src: element.image,
            caption: `${element.description}`,
            counter: element.counter,
          })
        })
        this.setState({ posts: posts })
        if (posts.length = 2) {
          return { ...this.state.slideItems, lg: 2 }
        }
        if (posts.length = 1) {
          return { ...this.state.slideItems, md: 1, lg: 1 }
        }
      }
    }).catch(error=>{

    })
  }

  items = () => {
    let items = []
    items = this.state.posts
    return (
      items.map((element, index) => {
        return (
          <Card key={index} className='s-card'>
            <CardImg className="border-secondary" top src={element.src} />
            <CardBody className="pt-2">
              <div className="d-block d-xl-flex justify-content-between ">
                <CardTitle className="my-0">{element.title}</CardTitle>
                <div> <span className="history text-muted">{element.date}</span></div>
              </div>
              <CardText className="text-muted" dangerouslySetInnerHTML={{ __html: (element.caption.length > 80) ? (element.caption.slice(0, 80)) : (element.caption) }}></CardText>
              <a className='text-danger ' href={`/news/post/?id=${element.id}`}><LineTitle color="text-secondary" content="Read More" /></a>
            </CardBody>
          </Card>
        )
      })
    )
  }
  SampleNextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={styles.nextBtn}
        style={{ ...style, display: "block", right: '78px', zIndex: '2' }}
        onClick={onClick}>
        <span><img type="button" className="mr-2" src={images.sliderIcons.rightBtn_2} /></span>
      </div>
    );
  }
  SamplePrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={styles.prevBtn}
        style={{ ...style, display: "block", right: '134px', zIndex: '2' }}
        onClick={onClick}>
        <span><img type="button" className="mr-2" src={images.sliderIcons.leftBtn_2} /></span>
      </div>
    );
  }

  render(props) {
    const settings = {
      className:'owlCarousel',
      dots: true,
      speed: 500,
      slidesToShow: this.state.slideItems.lg,
      slidesToScroll: this.state.slideItems.lg,
      initialSlide: 0,
      centerMode: true,
      infinite: this.state.posts.length > 2 ? true : false,
      prevArrow: <this.SamplePrevArrow />,
      nextArrow: <this.SampleNextArrow />,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: this.state.slideItems.lg,
            slidesToScroll: this.state.slideItems.lg,
            dots: true,
          }
        },

        {
          breakpoint: 800,
          settings: {
            slidesToShow: this.state.slideItems.md,
            slidesToScroll: this.state.slideItems.md,
            dots: true
          }
        },
        {
          breakpoint: 568,
          settings: {
            slidesToShow: this.state.slideItems.sm,
            slidesToScroll: this.state.slideItems.sm,
            dots: true
          }
        }
      ]
    }
    return (
      <section className={`${styles.mostViewed} position-relative overflow-hidden`}>
        <Container fluid className="h-auto px-0">
          <Wrapper>
            <Row className="my-5" className="align-items-end justify-content-start">
              <Col xs="12" md="6" className="mb-4 pl-0">
                <LineTitle className="container" color="text-danger" content="SKYMOVEX LOGISTICS" />
                <Heading color="text-dark" content={this.state.title} />
              </Col>
              <Col xs="12" md="6" className="mb-4">
                <div className={`${styles.showAllCarosel} d-flex justify-content-xs-center justify-content-md-end`}>
                  <p><a href='/news' className='text-muted'>show all</a></p>
                </div>
              </Col>
            </Row>
            <Row className="my-5" >
              <Slick style={{ width: '100%' }} {...settings}>
                {this.items()}
              </Slick>
            </Row>
          </Wrapper>
        </Container>
      </section>
    )
  }
}
export default MostViewed
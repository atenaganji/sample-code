import React, { Component } from 'react';
import {connect} from 'react-redux'
import Header from './header/header'
import Services from './services/services'
import NewsLetter from './newsletter/newsletter'
import About from './about/about'
import Footer from './../../components/footer/footer'
import MostViewed from './mostviewed/mostviewed'
import Values from './values/values'
import News from './news/news'

class Home extends Component {
  componentDidMount(){
    console.log(this.props.token)
  }
  componentDidUpdate(){
    
  }

  render() {
    return (
      <div className="App">
<Header />
<Values />
<Services/>
<About />
<MostViewed />
<News />
<NewsLetter />
<Footer  />

      </div>
    );
  }
}

const mapStateToProps=state=>{
  return{
    token:state.auth.token
  }
}
export default connect(mapStateToProps)(Home);
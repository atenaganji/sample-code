import React, {Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Wrapper from './../../../layouts/wrapper/wrapper'
import LineTitle from './../../../components/textsection/lineTitle'
import Heading from "./../../../components/textsection/heading"
import images from './../../../public/assets/images'
import CustomCard from './../../../components/customCards/customCards'
import axios from './../../../axios/axios'
import * as Urls from './../../../reqURL/url'
import Slick from 'react-slick'
import styles from './services.module.scss'

class Services extends Component {

  constructor(props) {
    super(props)
    this.state = {
      title: "Our Services",
      posts: [
        {
          id: '',
          src: images.services.air,
          date: 'September 3,2020',
          title: 'Air Freight',
          path: '/services/air',
          caption: `Skymovex offers you a smooth shipment between your partners around the world in a cost effective manner.
                  A door to door service, we support your shipment until it will be clear from customs and get in your hands
                  `,
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.services.road,
          path: '/services/road',
          date: 'September 3,2020',
          caption: `Faster than sea freight and cheaper than air freight.
                  Road Freight is one of the secure, cost effective and flexible method of cargo transportation all the time
                  `,
          title: 'Road Freight',
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.services.sea,
          path: '/services/sea',
          date: 'September 3,2020',
          title: 'Sea Freight',
          caption: `Skymovex suggested Sea Freight Services if you have a large volumes of cargo. It’s the best method of transporting the huge cargo in a cost effective ways especially when your shipment is not time sensitive. `,
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },
        {
          id: '',
          src: images.services.custom,
          path: '/services/custom',
          date: 'September 3,2020',
          title: 'Custom clearance',
          caption: `As an importer with huge sizes of shipments or owner of a small business with a small cargo, you will benefit to use our custom clearance services to clear your goods in a timely manner. Less time means less costs! Time is precious.`,
          counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        },


      ],
      slideItems: {
        sm: 1,
        md: 2,
        lg: 3
      }
    }

  }

  componentDidMount() {
    axios.get(Urls.institute.services).then(response => {
      if (response.data.length != 0 ) {
        let posts = []
        response.data.map((element, index) => {
          posts.push({
            id: element.id,
            title: element.title,
            date: element.date,
            src: element.image,
            path: `/services/post/?id=${element.id}`,
            caption: element.description,
            counter: element.counter,
          })

        })
        this.setState({ posts: posts })
               if (posts.length = 2) {
          return { ...this.state.slideItems, lg: 2 }
        }
        if (posts.length = 1) {
          return { ...this.state.slideItems, md: 1, lg: 1 }
        }
      }
    }).catch(error=>{

    })
  }


  items = () => {

    let items = []
    items = this.state.posts
     return (
      items.map((element, index) => {
        return (
          <div key={index}>
            <CustomCard path={element.path} src={element.src} title={element.title} overly_title={element.title} overly_des={element.caption}></CustomCard>
          </div>
        )
      })
    )

  }
  SampleNextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={styles.nextBtn}
        style={{ ...style, display: "block", right: '78px', zIndex: '2' }}
        onClick={onClick}>
        <span><img type="button" className="mr-2" src={images.sliderIcons.rightBtn_2} /></span>
      </div>
    );
  }
  SamplePrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={styles.prevBtn}
        style={{ ...style, display: "block", right: '134px', zIndex: '2' }}
        onClick={onClick}>
        <span><img type="button" className="mr-2" src={images.sliderIcons.leftBtn_2} /></span>
      </div>


    );
  }

  render(props) {
    const settings = {
      className:'owlCarousel',
      dots: true,
      speed: 500,
      initialSlide: 0,
      centerMode: true,
      infinite: this.state.posts.length > 2 ? true : false,
      slidesToShow: this.state.slideItems.lg,
      slidesToScroll: this.state.slideItems.lg,
      prevArrow: <this.SamplePrevArrow />,
      nextArrow: <this.SampleNextArrow />,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: this.state.slideItems.lg,
            slidesToScroll: this.state.slideItems.lg,
            dots: true
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: this.state.slideItems.md,
            slidesToScroll: this.state.slideItems.md,
            dots: true
          }
        },
        {
          breakpoint: 568,
          settings: {
            dots: true,
            slidesToShow: this.state.slideItems.sm,
            slidesToScroll: this.state.slideItems.sm
          }
        }
      ]
    }
    return (
      <section className={`${styles.services} position-relative overflow-hidden`}>
        <Container fluid className="h-auto">
          <Wrapper>
            <Row className="my-5" className="align-items-end justify-content-start">
              <Col xs="12" md="6" className="mb-4 pl-0">
                <LineTitle className="container" color="text-danger" content="SKYMOVEX LOGISTICS" />
                <Heading color="text-dark" content={this.state.title} />
              </Col>
              <Col xs="12" md="6" className="mb-4">
                <div className={`${styles.showAllCarosel} d-flex  justify-content-xs-center justify-content-md-end`}>
                </div>
              </Col>
            </Row>
            <Row className="my-5" >
              <Slick style={{ width: '100%' }} {...settings}>
                {
                  this.items()
                }
              </Slick>
            </Row>
          </Wrapper>
        </Container>
      </section>
    )
  }
}
export default Services
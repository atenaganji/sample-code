import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import style from './header.module.scss'
import Navbar from './../../../components/navbar/navbaritems';
import TS from './../../../components/textsection/textsection'
import TitleImg from './../../../components/titleImg/titleImg'
import Wrapper from './../../../layouts/wrapper/wrapper'
import Scroll from './../../../components/scroll/scroll'
//import video from './../../../public/assets/video/about.mp4'
import {Player} from 'video-react';
import Link from 'next/Link'
import axios from './../../../axios/axios'
import * as Urls from './../../../reqURL/url'

const video = require('../../../public/assets/video/about.mp4')

class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            heading: "Competetive services,compelete solution,best price available",
            praph: 'Skymovex is a leading independent logistic provider in the shipping and forwarding industry based in Turkey. We provide freight forwarding services all around the globe. Our reliable team make the process of moving your goods simple, stress free and efficient.',
            brand: "SKYMOVEX LOGISTICS",
            video: video
        }
    }

    componentDidMount() {
        axios.get(Urls.home.videoForHeader).then(response => {
            if (Object.keys(response.data).length !== 0) {
                let video = ''
                video = response.data.video
                this.setState({video: video})
            }
        }).catch(error=>{

        })
        //document.querySelector('.header-section video').play()
    }

    render() {

        return (
            <section className={`${style.header} .header-section position-relative overflow-hidden`}>
                <Navbar color='navbar-dark'/>
                <Container fluid>
                    <Wrapper>
                        <Row className="h-100 mt-5 d-flex align-items-center">
                            <Col xs="12" sm="12" md="6" className="text-left py-5 px-4 ">
                                <TS colorTitle="text-light" lineTitle="SKYMOVEX LOGISTICS" heading={this.state.heading}
                                    colorHeading="text-light" colorParaph="#f0f0f0" paraph={this.state.praph}/>
                                <Link href='/tracking'>
                                    <a className="tracking d-inline-block my-3">Tracking</a>
                                </Link>
                                <Scroll/>
                            </Col>
                            <Col xs="12" sm="12" md="6" className=''>
                                <Player key={this.state.video} className={style.videoHeader} autoPlay muted>
                                    <source src={this.state.video}/>
                                </Player>
                                <TitleImg class={`${style.titleImg} d-none d-lg-block `} title={this.state.brand}
                                          colorTitle="text-light" des="World wide logistics service"
                                          colorDes="text-light"/>
                            </Col>
                        </Row>
                    </Wrapper>
                </Container>
            </section>
        )
    }
}

export default Header;
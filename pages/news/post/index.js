import React, {Component} from 'react';
import {Col, Row,} from 'reactstrap';
import {Container} from 'reactstrap'
import NavBar from './../../../components/navbar/navbaritems'
import Footer from './../../../components/footer/footer'
import images from './../../../public/assets/images'
import styles from './post.module.scss';
import axios from './../../../axios/axios'
import Link from 'next/link'
import * as Urls from './../../../reqURL/url'

class Post extends Component {

    constructor(props) {

        super(props)
        this.state = {
            id: '',
            service: '',
            services: [
                {
                    name: '',
                    title: '',
                    image: ""

                }
            ],
            post: {
                id: '',
                src: images.defaultImages.bg_1,
                date: '',
                title: 'Lorem ipsum dolor sit amet',
                caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                counter: ''
            },
        }
    }

    componentDidMount() {
        const query = new URLSearchParams(window.location.search)
        let id = query.entries().next().value[1]
        this.setState({id: id})
        axios.get(Urls.institute.news + id).then(res => {
            let posts = {}
            posts.id = res.data.id
            posts.title = res.data.title
            posts.date = new Date(res.data.date).toLocaleDateString()
            posts.src = res.data.image
            posts.caption = `${res.data.description}`
            posts.counter = res.data.counter
            this.setState({post: posts})
            console.log(this.state.posts)
        }).catch(error => {

        })
    }

    render(props) {
        return (
            <section className={`${styles.post}  light-nav position-relative`}>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>
                <div>
                    <Container className='mt-5 pt-3 px-5 h-auto'>
                        <Row className='text-center mt-md-5 pt-5'>
                            <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                                <Link href='/news'>
                                    <a className='text-muted'>&#10094; News</a>
                                </Link>
                            </div>
                            <Col sm='12' md='12'>
                                <h1>
                                    {(this.state.post.title) ? (this.state.post.title) : null}
                                </h1>
                                <p className='text-danger font-weight-bold'>
                                    {this.state.post.date}
                                </p>
                            </Col>
                            <Col sm='12' md='12'>
                                <img className={styles.img} src={this.state.post.src}>
                                </img>
                                <p className='border-bottom text-secondary mx-4 pt-4'> SKYMOVEXLOGISTICS</p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='mx-4' dangerouslySetInnerHTML={{__html: this.state.post.caption}}></Col>
                        </Row>
                    </Container>
                </div>
                <div className='mt-5 py-5'>
                    <Footer/>
                </div>

            </section>
        )
    }
}

export default Post
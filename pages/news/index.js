import React, {Component} from 'react';
import NavBar from './../../components/navbar/navbaritems'
import Footer from './../../components/footer/footer'
import styles from './news.module.scss'
import Slick from "react-slick";
import images from './../../public/assets/images'
import LineTitle from './../../components/textsection/lineTitle'
import axios from './../../axios/axios'
import Link from 'next/link'
import {
    Container, Col, Row,
} from 'reactstrap';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle
} from 'reactstrap';
import * as Urls from './../../reqURL/url'

class NewsPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items: [
                {
                    id: 1,
                    src: '',
                    date: 'September 3,2020',
                    altText: 'Lorem ipsum dolor sit amet',
                    caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    id: 2,
                    src: '',
                    date: '',
                    altText: 'Slide 2',
                    caption: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
            ],
            posts: [
                {
                    id: '',
                    src: images.defaultImages.bg_2,
                    date: 'September 3,2020',
                    title: 'Lorem ipsum dolor sit amet',
                    caption: '',
                    counter: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                },
                {
                    src: images.defaultImages.bg_1,
                    date: 'September 3,2020',
                    title: 'Lorem ipsum dolor sit amet',
                    caption: ''
                },
                {
                    src: images.defaultImages.bg_3,
                    date: '',
                    title: '',
                    caption: ''
                },
                {
                    src: images.defaultImages.bg_1,
                    date: '',
                    title: '',
                    caption: ''
                },
            ]
        }
    }

    componentDidMount() {
        axios.get(Urls.institute.news).then(res => {
            if (res.data.length != 0) {
                let posts = []
                res.data.map((element, index) => {
                    posts.push({
                        id: element.id,
                        title: element.title,
                        date: element.date,
                        src: element.image,
                        caption: `${element.description}`,
                        counter: element.counter,
                    })
                    this.setState({posts: posts})
                    console.log(this.state.posts)
                })
            }
        }).catch(error => {

        })
    }

    posts = () => {
        return (
            this.state.posts.map((element, index) => {
                return (
                    <Col className='pt-5' sm='12' md='6' lg='4' xl='3' key={index}>
                        <Card className={styles.spCard}>
                            <CardImg top width="100%" src={element.src} alt="Card image cap"/>
                            <CardBody>
                                <div><p className='text-danger'>{element.date}</p></div>
                                <CardTitle>{element.title}</CardTitle>

                                <CardText
                                    dangerouslySetInnerHTML={{__html: (element.caption.length > 80) ? (element.caption.slice(0, 80)) : (element.caption)}}></CardText>
                                <a href={`/news/post/?id=${element.id}`}><LineTitle color="text-dark"
                                                                                    content="Read More"/></a>
                            </CardBody>
                        </Card>
                    </Col>
                )
            })
        )
    }

    items = () => {
        let items = []
        if (this.state.posts.length > 3)
            items = this.state.posts.slice(0, 3)
        else {
            items = this.state.posts
        }
        return (
            items.map((element, index) => {
                return (<div key={index} className={styles.card}>
                    <div className="img-container">
                        <div className={styles.shodowImg}>
                            <img src={element.src} alt=""
                                 className={styles.cardImg}/>
                        </div>
                    </div>
                    <div className={`${styles.cardImgOverlay} overlay-img`}>

                        <div className={styles.title}>
                            <p className='text-muted font-weight-bold'>{element.date}</p>
                            <h2 className="card-title">{element.title}</h2>
                            <p dangerouslySetInnerHTML={{__html: (element.caption.length > 120) ? (element.caption.slice(0, 120)) : (element.caption)}}
                               className='text-light'></p>
                        </div>

                    </div>

                </div>)
            })
        )


    }

    SampleNextArrow = (props) => {
        const {className, style, onClick} = props;
        return (
            <div
                className={styles.arrows}
                style={{...style, display: "block", right: '51px', zIndex: '2'}}
                onClick={onClick}>
                <img src={images.sliderIcons.rightBtn_3} alt=""/>
            </div>


        );
    }
    SamplePrevArrow = (props) => {
        const {className, style, onClick} = props;
        return (
            <div
                className={styles.arrows}
                style={{...style, display: "block", left: '30px', zIndex: '2'}}
                onClick={onClick}>
                <img src={images.sliderIcons.leftBtn_3} alt=""/>
            </div>


        );
    }


    render(props) {

        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: <this.SamplePrevArrow/>,
            nextArrow: <this.SampleNextArrow/>,


        };
        return (
            <section className='newspage-section light-nav position-relative'>
                <Container fluid className='h-auto'><Row><NavBar color='navbar-light'/></Row> </Container>
                <Container className='mt-5 pt-3  h-auto'>
                    <Row className='text-center mt-md-5 pt-5'>
                        <div className='mt-5 pt-5 pl-2 pl-md-0 '>
                            <Link href='/'>
                                <a className='text-muted'>&#10094; HOME</a>
                            </Link>
                        </div>
                        <Col sm='12' md='12'>
                            <h1>
                                News and Events
                            </h1>

                            <p className='text-danger font-weight-bold'>
                                SKYMOVEXLOGISTICS
                            </p>
                        </Col>
                        <Col className='px-0' sm='12' md='12'>
                            <div>
                                <Slick {...settings}>
                                    {this.items()}
                                </Slick>

                            </div>
                        </Col>
                    </Row>

                    <Row className='my-5 px-0 px-md-5'>
                        {this.posts()}
                    </Row>
                </Container>
                <Footer/>
            </section>
        )
    }
}

export default NewsPage;
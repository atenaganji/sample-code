import React, { Component, Suspense } from 'react';
import { Col, Row, Button, Form } from 'reactstrap';
import { Container } from 'reactstrap'
import Toast from './../../components/alertToastify/alert'
import Heading from './../../components/textsection/heading'
import GroupForms from "../../components/form/inputForms/inputForms"
import NavBar from './../../components/navbar/navbaritems'
import Footer from './../../components/footer/footer'
import style from './contactUs.module.scss';
import LineTitle from './../../components/textsection/lineTitle'
import axios from './../../axios/axios';
import * as Urls from './../../reqURL/url'
class ContactUs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fields:
                [
                    {
                        type: "text",
                        name: "name",
                        labelvalue: "Name",
                        grid: 4,
                        placeholder: "",
                        required: true,
                        validation: "Only English letters are allowed",
                        pattern: "",
                        value: ""
                    },
                    {
                        type: "text",
                        name: "email",
                        labelvalue: "Email",
                        grid: 4,
                        placeholder: "",
                        required: true,
                        validation: "Only English letters are allowed",
                        pattern: "",
                        value: "",
                    },
                    {
                        type: "text",
                        name: "phone",
                        labelvalue: "Tel",
                        grid: 4,
                        placeholder: "",
                        required: true,
                        validation: "Only English numbers are allowed",
                        pattern: "",
                        value: "",
                    },
                    {
                        type: "textarea",
                        name: "message",
                        labelvalue: "Message",
                        grid: 12,
                        placeholder: "",
                        required: true,
                        validation: "Only English letters are allowed",
                        pattern: "",
                        value: "",
                    },
                ],
            support: {
                email: '',
                phone: '',
                address: '',
                map: '',
            },
            data: []
        }
    }
    changedData = (event, name) => {
        let fieldIndex = this.state.fields.findIndex(field => {
            return field.name === name
        })
        let field = { ...this.state.fields[fieldIndex] }
        if (name === 'phone') {
            field.value = event;
        }
        else { field.value = event.target.value; }
        const fields = [...this.state.fields];
        fields[fieldIndex] = field;
        this.setState({ fields: fields });
    }
    componentDidMount() {
        axios.get(Urls.institute.contactus).then((response) => {
            let support = {
                email: response.data.email,
                phone: response.data.phone_number,
                address: response.data.address,
                map: response.data.map
            }
            this.setState({ support: support })
            }).catch((error) => {
            console.log(error.response)
        })
    }
    componentDidUpdate() {
        let inputs = document.querySelectorAll(".contactus-section input");
        let arrayInputs = [...inputs];
        for (let i = 0; i < arrayInputs.length; i++) {
            if (inputs[i].value.length == 0) {
                inputs[i].classList.add("empty-input")
            }
            else {
                inputs[i].classList.remove("empty-input");
                inputs[i].classList.add("shadow");
                inputs[i].classList.add("full-input");
            }
        }
    }
    items() {
        return (
            this.state.fields.map((element, index) => {
                return (
                    <Col key={index} md={element.grid}>
                        <GroupForms key={index} validation={element.validation} value={element.value} type={element.type} name={element.name} placeholder={element.placeholder} labelValue={element.labelvalue} required={element.required} change={(event) => { this.changedData(event, element.name) }} />
                    </Col>
                )
            })
        )
    }

    handleSubmit = event => {
        event.preventDefault();
        const jsonData = {
            "name": "",
            "email": "",
            "phone_number": "",
            "message": ""
        }

        this.state.fields.map((element, index) => {
            if (element.name == "email") {
                jsonData.email = element.value.toLowerCase();
            }
            if (element.name == "name") {
                jsonData.name = element.value
            }
            if (element.name == "phone") {
                jsonData.phone_number = '+' + element.value
            }
            if (element.name == "message") {
                jsonData.message = element.value
            }

        });

        axios.post(Urls.institute.contactus, jsonData, {
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then(response => {
                let p = document.getElementById("error-response");
                p.textContent = "";
                let success = document.getElementById("success-response");
                success.textContent = "";
                if (response.status == 200 || response.status == 201) {
                    Toast('success', response.data.message, 'bottom-right', '5000')
                    let inputCodes = document.querySelectorAll('.contactus-section input ,.contactus-section textarea ');
                    for (const input of inputCodes) {
                        input.value = ''
                    }
                }

            })
            .catch(error => {
                let p = document.getElementById("error-response");
                if (error.response && error.response.status && /^5\d{2}$/.test(error.response.status.toString())) {
                    Toast('error', 'Server connection error!', 'bottom-right', '5000')
                }
                else if (error.response && error.response.data && error.response.data !== undefined) {
                    Object.keys(error.response.data).forEach((key, index) => {
                        Toast('error', error.response.data[key] + '', 'bottom-right', '7000')
                    })
                }
                else {
                    Toast('error', 'Server connection error!', 'bottom-right', '5000')
                }
            });

    }

    render() {
        const mapStyles = {
            width: '100%',
            height: '100%',
        };
        return (
            <Suspense fallback={<div>Loading...</div>}>
                <section className={`${style.contactUs} position-relative`}>
                    <NavBar color='navbar-dark' />
                    <Container className={style.headContainer}>
                        <Row>
                            <div className={style.head}>
                                <Heading color="text-light" content="Still have a Question?" />
                                <LineTitle color="text-light" content="Contact Us" />
                            </div>
                        </Row>
                    </Container>
                    <Container className={`${style.formContainer} shadow`}>
                        <div>
                            <h6>Sender</h6>
                            <p className="text-muted">Please Fill Out The Qick Form(Below) And WE Will Be In Touch With You Quickly</p>
                            <p className="text-muted">If Your Enquiry Is About Shipping A Cargo,Please Write The Cargo Details And Origin/Destination With Your Prefer Method Of Shipping.</p>
                            <br></br>
                            <p className="text-muted">For Other Inquiries Please Mails Us At: <span className="text-dark font-weight-bold">{this.state.support.email}</span></p>
                            <p className="text-muted" >Urgent Inquiries Calls Us At: <span className="text-dark font-weight-bold">{this.state.support.phone}</span> </p>
                            <p className="text-muted" >Our Address is: <span className="text-dark font-weight-bold">{this.state.support.address}</span> </p>
                        </div>
                        {(this.state.support.map !== null && this.state.support.map !== "") ?
                            <Row className='py-2'>
                                <Col sm='12' md='8' lg='6'> <iframe src={this.state.support.map} frameBorder="0" style={{ border: '0', width: '100%', height: '250px' }} allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe></Col>
                            </Row>
                            :
                            <Row className='py-2'>
                                <Col sm='12' md='8' lg='6'>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1790.4460672107753!2d28.623370142626005!3d40.999885092639175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b55f41fe27e811%3A0xf32755c21ff7c3c6!2sDurin%20Ajans!5e0!3m2!1sen!2s!4v1604426314255!5m2!1sen!2s" frameBorder="0" style={{ border: '0', width: '100%', height: '250px' }} allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>
                                </Col>
                            </Row>
                        }

                        <Form className="py-5" onSubmit={this.handleSubmit}>
                            <h6>Contact Form</h6>
                            <Row> {this.items()}  </Row>
                            <Button type="submit" outline color="white" className="tracking">Send Message</Button>
                        </Form>
                        <p id="error-response" className="text-danger"></p>
                        <p id="success-response" className="text-success"></p>
                        </Container>
                    <Footer />
                </section>
            </Suspense>)
    }
}
export default ContactUs
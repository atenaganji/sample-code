import React, { Component } from 'react';
import style from './wrapper.module.scss'

class Wrapper extends Component {
    render() {
        return (
            <div className={style.wrapper}>
                {this.props.children}
            </div>
        )
    }
}
export default Wrapper 